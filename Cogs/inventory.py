import discord
from discord.ext import commands
import asyncio
import json
import datetime
import sys
import sqlite3

intents = discord.Intents.all()
client = commands.Bot(command_prefix = "!udp ", case_insensitive = True, intents = intents)


class InventoryCog(commands.Cog, name="Inventory"):

    def __init__(self, bot):
        self.bot = bot
        self._last_member= None
    
  
    #Desplasar inventario  
    @client.command()
    async def inv(self, ctx, *, member: discord.Member=None):
        if not member:
            member = ctx.author
        
        udp_worlddb = sqlite3.connect('udp_world.sqlite')
        cursor1 = udp_worlddb.cursor()
        cursor1.execute(f"SELECT id_discord FROM JUGADOR WHERE id_discord = {member.id}")
        result=cursor1.fetchone()
        if result is None:
            await ctx.send("Usted aun no pertenece a Udp_World")
            return
        else:
            #print(f"{member.guild.id} paso")
            cursor1.execute(f"SELECT medicamentos FROM INVENTARIO,JUGADOR WHERE INVENTARIO.id_discord = {member.id}")
            Medicamento = cursor1.fetchone()
            #print(Medicamento)
            cursor1.execute(f"SELECT computadora FROM INVENTARIO,JUGADOR WHERE INVENTARIO.id_discord = {member.id}")
            Computadora = cursor1.fetchone()
            cursor1.execute(f"SELECT apuntes FROM INVENTARIO,JUGADOR WHERE INVENTARIO.id_discord = {member.id}")
            Apuntes = cursor1.fetchone()
            cursor1.execute(f"SELECT comida FROM INVENTARIO,JUGADOR WHERE INVENTARIO.id_discord = {member.id}")
            Comida = cursor1.fetchone() 
            embed=discord.Embed(title="Inventario", description="Legendarios usuarios de la udp ,requieren legendarios items para sus travesias.", color=discord.Color.red())
            embed.set_author(name=f"{member.name}")
            embed.set_thumbnail(url="https://i.pinimg.com/originals/a1/3d/0a/a13d0a39629a49fd2b7585ce229ff398.jpg")
            embed.add_field(name="Medicamento:", value=Medicamento[0], inline=True)
            embed.add_field(name="Computadora:", value=Computadora[0], inline=True)
            embed.add_field(name="Apuntes:", value="!udp catedra", inline=True)
            embed.add_field(name="Comida:", value=Comida[0], inline=True)
            embed.set_footer(icon_url= 'https://pbs.twimg.com/profile_images/1296920293873680385/KiuKejIt_400x400.jpg' , text="Para lista de comandos, ingresa !udp ayuda")
            await ctx.send(embed=embed)

def setup(client):
    client.add_cog(InventoryCog(client))
    print('Welcome is loaded Inventario')