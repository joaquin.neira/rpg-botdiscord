import discord
from discord.ext import commands
import asyncio
import json
import datetime
import sys
import sqlite3

intents = discord.Intents.all()
client = commands.Bot(command_prefix = "!udp ", case_insensitive = True, intents = intents)


class RankCog(commands.Cog, name="Rank"):

    def __init__(self, bot):
        self.bot = bot
        self._last_member= None


    #Crear usuario  
    @commands.command()
    async def rank(self,ctx,*,member: discord.Member=None):
        if not member:
            member = ctx.author

        udp_worlddb = sqlite3.connect('udp_world.sqlite')
        cursor = udp_worlddb.cursor()
        cursor.execute("SELECT nombre,nivel FROM USUARIOS ORDER BY nivel DESC")
        rows = cursor.fetchall()    
        i=0
        L=['1️⃣','2️⃣','3️⃣','4️⃣','5️⃣','6️⃣','7️⃣','8️⃣','9️⃣','🔟']
        embed=discord.Embed(title=f"Ranking mejores jugadores", description = "Los mejores jugadores de UDP World.",color=discord.Color.red())
        embed.set_footer(icon_url= 'https://pbs.twimg.com/profile_images/1296920293873680385/KiuKejIt_400x400.jpg' , text="Para lista de comandos, ingresa !udp ayuda")

        for data in rows:
            embed.add_field(name=f"{L[i]}     {data[0]}     Nivel: {data[1]}", value='\u200b', inline=False)
            i+=1
            if i==10:
                break

        await ctx.send(embed=embed)
        udp_worlddb.commit()
        cursor.close()
        udp_worlddb.close()
        
          


def setup(client):
    client.add_cog(RankCog(client))
    print('Welcome is loaded Rank')