import discord
from discord.ext import commands
from discord import Member
import asyncio
import json
import datetime
import sys
import sqlite3
import os.path

intents = discord.Intents.all()
client = commands.Bot(command_prefix = "!udp ", case_insensitive = True, intents = intents)

class ProfileCog(commands.Cog, name="Profile"):

    def __init__(self, bot):
        self.bot = bot
        self._last_member= None
  
    @client.command()
    async def profile(self, ctx,* , user: discord.Member = None):
        usuariosdb = sqlite3.connect('udp_world.sqlite')
        cursor = usuariosdb.cursor()
        cursor.execute(f"SELECT nombre,nivel,carrera,id_discord FROM USUARIOS WHERE id_discord={ctx.author.id}")
        perfil = cursor.fetchone()        
        embed = discord.Embed(title = f"Perfil de {ctx.author.name}" , color = discord.Color.red())
        embed.add_field(name = "Nivel" ,value = f"{perfil[1]}")
        embed.add_field(name = "Carrera" , value = f"{perfil[2]}", inline = False)
        embed.add_field(name= "ID Discord", value = f"{perfil[3]}")
        embed.set_thumbnail(url = ctx.author.avatar_url)
        embed.set_footer(icon_url= 'https://pbs.twimg.com/profile_images/1296920293873680385/KiuKejIt_400x400.jpg' , text="Para lista de comandos, ingresa !udp ayuda")
        usuariosdb.commit()
        cursor.close()
        usuariosdb.close()
        await ctx.send(embed = embed)

                
def setup(client):
    client.add_cog(ProfileCog(client))
    print('Profile is loaded')