import discord
from discord.ext import commands
import asyncio
import json
import datetime
import sys
import sqlite3

intents = discord.Intents.all()
client = commands.Bot(command_prefix = "!udp ", case_insensitive = True, intents = intents)


class CatedraCog(commands.Cog, name="Catedra"):

    def __init__(self, bot):
        self.bot = bot
        self._last_member= None


    #Crear usuario  
    @commands.command()
    async def catedra(self,ctx):

        embed=discord.Embed(title=f"Catedra de Redes de Datos", url="https://www.youtube.com/playlist?list=PLomN84AdULIBcoI8Rb98dnompliIktJk9",description = "Amplía tus conocimientos sobre el internet y sus sistemas!",color= discord.Color.red())
        embed.set_thumbnail(url="https://www.tecnocomprascr.com/wp-content/uploads/2014/07/Redes-inteligentes.png")
        await ctx.send(embed=embed)
      
        embed=discord.Embed(title=f"Catedra de Programación", url="https://udpiler.com/?/Main/world/1",description = "Fortalece tus conocimientos básicos de la programación!",color= discord.Color.red())
        embed.set_thumbnail(url="https://dev-res.thumbr.io/libraries/14/04/09/lib/1533172648445_1.jpg?size=854x493s&ext=jpg")
        await ctx.send(embed=embed)
        
        embed=discord.Embed(title=f"Catedra de Contabilidad y Costos", url="https://www.youtube.com/playlist?list=PLz8UvxkqGuJvwu2uGq5iQ8c6pciytb5oC",description = "Desarrolla tus habilidades lógicas contables!",color= discord.Color.red())
        embed.set_thumbnail(url="https://www.investopedia.com/thmb/665PtRawKbbUnRXhyLEaP1lSmoE=/1615x1080/filters:fill(auto,1)/accountant-calculator-accounting-graphs-career-business-1449659-pxhere.com-072d54485e09467292a7fb73fa1761df.jpg")
        await ctx.send(embed=embed)


def setup(client):
    client.add_cog(CatedraCog(client))
    print('Welcome is loaded Catedra')