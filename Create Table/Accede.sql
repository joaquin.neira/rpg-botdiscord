-- Table: public.Accede

-- DROP TABLE public."Accede";

CREATE TABLE public."Accede"
(
    codigo_ramo character varying(9) COLLATE pg_catalog."default" NOT NULL,
    id_discord character varying(9) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Accede_pkey" PRIMARY KEY (codigo_ramo, id_discord),
    CONSTRAINT fk_codigo_ramo FOREIGN KEY (codigo_ramo)
        REFERENCES public."RAMOS" (codigo_ramo) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT fk_id_discord2 FOREIGN KEY (id_discord)
        REFERENCES public."JUGADOR" (id_discor) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."Accede"
    OWNER to postgres;