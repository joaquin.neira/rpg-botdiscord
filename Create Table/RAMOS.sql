-- Table: public.RAMOS

-- DROP TABLE public."RAMOS";

CREATE TABLE public."RAMOS"
(
    codigo_ramo character varying(9) COLLATE pg_catalog."default" NOT NULL,
    nombre_ramo text COLLATE pg_catalog."default",
    preguntas_control text COLLATE pg_catalog."default",
    "Materia_Ramo" text COLLATE pg_catalog."default",
    CONSTRAINT "Ramos_pkey" PRIMARY KEY (codigo_ramo)
)

TABLESPACE pg_default;

ALTER TABLE public."RAMOS"
    OWNER to postgres;