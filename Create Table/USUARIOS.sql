-- Table: public.USUARIO

-- DROP TABLE public."USUARIO";

CREATE TABLE public."USUARIO"
(
    "ID_Perfil" integer NOT NULL,
    nombre character varying(25) COLLATE pg_catalog."default",
    nivel integer,
    genero text COLLATE pg_catalog."default",
    carrera text COLLATE pg_catalog."default",
    id_discord character varying(9) COLLATE pg_catalog."default",
    CONSTRAINT usuarios_pkey PRIMARY KEY ("ID_Perfil"),
    CONSTRAINT fk_id_discord FOREIGN KEY (id_discord)
        REFERENCES public."JUGADOR" (id_discor) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."USUARIO"
    OWNER to postgres;