-- Table: public.INVENTARIO

-- DROP TABLE public."INVENTARIO";

CREATE TABLE public."INVENTARIO"
(
    id_discord character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    id_invetario character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT NULL::character varying,
    medicamentos character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    computadora character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    apuntes character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    comida character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    CONSTRAINT "INVENTARIO_pkey" PRIMARY KEY (id_invetario),
    CONSTRAINT fk_id_discord FOREIGN KEY (id_discord)
        REFERENCES public."JUGADOR" (id_discor) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."INVENTARIO"
    OWNER to postgres;