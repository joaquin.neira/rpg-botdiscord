-- Table: public.EXAMEN

-- DROP TABLE public."EXAMEN";

CREATE TABLE public."EXAMEN"
(
    id_ramo character varying(9) COLLATE pg_catalog."default",
    id_examen character varying(9) COLLATE pg_catalog."default" NOT NULL,
    preg_examen text COLLATE pg_catalog."default",
    CONSTRAINT "EXAMEN_pkey" PRIMARY KEY (id_examen),
    CONSTRAINT fk_id_ramo FOREIGN KEY (id_ramo)
        REFERENCES public."RAMOS" (codigo_ramo) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."EXAMEN"
    OWNER to postgres;