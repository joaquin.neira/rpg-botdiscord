#importa librerias de Discord
import discord
from discord.ext import commands
import asyncio
import json
import datetime
import sys
import traceback
import sqlite3
import os
import time
#prefijo
intents = discord.Intents.all()
client = commands.Bot(command_prefix = "!udp ", case_insensitive = True, intents = intents)

# Load cogs
path = os.path.realpath(__file__)
path = path.replace('\\', '/')
path = path.replace('main.py', 'Cogs')
initial_extensions = os.listdir(path)
try:
    initial_extensions.remove("__pycache__")
except:
    pass
print(initial_extensions)
initial_extensions3 = []
for initial_extensions2 in initial_extensions:
    initial_extensions2 = "Cogs." + initial_extensions2
    initial_extensions2 = initial_extensions2.replace(".py", "")
    initial_extensions3.append(initial_extensions2)


if __name__ == '__main__':
    for extension in initial_extensions3:
        try:
            client.load_extension(extension)
        except Exception as e:
            print(f'Failed to load extension {extension}.', file=sys.stderr)

        
@client.command()
async def hellothere(ctx):
    embed=discord.Embed()
    embed.set_image(url="https://i.imgur.com/75TEk4A.gif")
    await ctx.send(embed=embed)

#Crear usuario  
@client.command()
async def create(ctx,*,member: discord.Member=None):
    if not member:
        member = ctx.author

    udp_wolrddb = sqlite3.connect('udp_world.sqlite')
    cursor = udp_wolrddb.cursor()
    cursor.execute(f"SELECT id_discord FROM JUGADOR WHERE id_discord = {member.id}")
    result = cursor.fetchone()
    print(result)
    embed_created=discord.Embed(title = "Usuario creado!", description = "Usa !udp profile para ver tu perfil!", color=discord.Color.red())
    embed_created.add_field(name = "Bienvenido a UDP World!", value = '\u200b', inline=False)
    embed_created.set_footer(icon_url= 'https://pbs.twimg.com/profile_images/1296920293873680385/KiuKejIt_400x400.jpg' , text="Para lista de comandos, ingresa !udp ayuda")
    embed_registered=discord.Embed(title = "Ya tienes una cuenta creada!", description = " ", color=discord.Color.red()) 
    embed_registered.add_field(name="Usa !udp profile para ver tu perfil.", value = '\u200b', inline=False)
    embed_registered.set_footer(icon_url= 'https://pbs.twimg.com/profile_images/1296920293873680385/KiuKejIt_400x400.jpg' , text="Para lista de comandos, ingresa !udp ayuda")
    if result is None:
        embed_career=discord.Embed(title = "Escoge tu carrera!", description="Reacciona a una de las carreras en ingeniería para registrarte:", color=discord.Color.red())
        embed_career.set_footer(icon_url= 'https://pbs.twimg.com/profile_images/1296920293873680385/KiuKejIt_400x400.jpg' , text="Para lista de comandos, ingresa !udp ayuda")
        emoji_a = '💾'
        emoji_b = '🏭'
        emoji_c = '🏗️'
        embed_career.add_field(name=f"{emoji_a}   Informática y Telecomunicaciones", value='\u200b', inline=False)
        embed_career.add_field(name=f"{emoji_b}   Industrial", value='\u200b', inline=False)
        embed_career.add_field(name=f"{emoji_c}   Obras Civiles", value='\u200b', inline=False)
        car_emb = await ctx.send(embed=embed_career)
        await car_emb.add_reaction(emoji_a)
        await car_emb.add_reaction(emoji_b)
        await car_emb.add_reaction(emoji_c)
        
        def check(reaction,user):
            return user == ctx.author
        try:
            reaction, user = await client.wait_for('reaction_add', timeout=60.0, check=check)
            if reaction.emoji == emoji_a:
                career = "Informática y Telecomunicaciones"
            elif reaction.emoji == emoji_b:
                career = "Industrial"
            else:
                career = "Obras Civiles"
        except asyncio.TimeoutError:
            embedf=discord.Embed(title = f"No has ingresado tu carrera, inténtalo de nuevo.", color = discord.Color.red())
            await ctx.send(embed=embedf)

        insert_stmt1 = ("INSERT INTO JUGADOR (id_discord) VALUES (?)")   
        insert_stmt2 = ("INSERT INTO USUARIOS (ID_Perfil,nombre,nivel,carrera,id_discord) VALUES (?,?,?,?,?)")     
        insert_stmt3 = ("INSERT INTO INVENTARIO (ID_Discord,ID_Invetario,Medicamentos,Computadora,Apuntes,Comida) VALUES (?,?,?,?,?,?)")    
        data1 = (f"{member.id}")
        data2 = (f"{member.id}",f"{member.name}",0,career,f"{member.id}")
        data3 = (f"{member.id}",f"{member.id}","MENTIX","acer","TEXT","Completo") 
        print(f"Usuario creado ID: {member.id}")
        #Cursor.execute
        cursor.execute(insert_stmt1, (data1,))
        cursor.execute(insert_stmt2, data2)
        cursor.execute(insert_stmt3, data3)
        await ctx.send(embed=embed_created)
    else:
        await ctx.send(embed=embed_registered)
    #Commit
    udp_wolrddb.commit()
    #Cursor.close
    cursor.close()
    #DB close
    udp_wolrddb.close()

@client.command()
async def ayuda(ctx):
    channel=ctx.channel
    embed = discord.Embed(title="Comandos disponibles en UDP World", description="Envía estos comandos en el chat de texto para ejecutarlos",color=0xFF0000)
    embed.add_field(name="**!udp create**" , value="Crea tu perfil. Debes tener un perfil para ejecutar los otros comandos.", inline = False)
    embed.add_field(name="**!udp profile**", value="Muestra tu perfil, que incluye tu nivel, experiencia actual y tu ranking", inline =False)
    embed.add_field(name="**!udp inv**", value = "Muestra la lista de objetos de tu inventario. (***WIP***)", inline = False)
    embed.add_field(name="**!udp rank**", value = "Muestra el Top 10 de posiciones con todos los usuarios registrados", inline =False)
    embed.add_field(name="**!udp test**", value = "Muestra las evaluaciones disponibles.", inline=False)
    embed.add_field(name= "**!udp test catedra**", value="Provee acceso al contenido de los ramos disponibles", inline=False)
    embed.add_field(name="**!udp hellothere**", value = "😉", inline = False)
    embed.set_footer(icon_url= 'https://pbs.twimg.com/profile_images/1296920293873680385/KiuKejIt_400x400.jpg' , text="Para lista de comandos, ingresa !udp ayuda")
    await channel.send(embed=embed)

#Test Contabilidad y Costos
@client.command()
async def test(ctx):
    member = ctx.author
    udp_worlddb = sqlite3.connect('udp_world.sqlite')
    cursor = udp_worlddb.cursor()
    channel = ctx.channel
    cursor.execute(f"SELECT nivel FROM USUARIOS WHERE id_discord={member.id} ")
    nivel =cursor.fetchone()
    embed=discord.Embed(title=f"Evaluaciones :", description = f"Para ingresar una solo ejecuta uno de los siguientes comando.",color=0xe65ba5)
    embed.add_field(name=f"1️⃣ ---> Contabilida y Costos.", value ='\u200b', inline=False)
    embed.add_field(name=f"2️⃣ ---> Programación.", value='\u200b', inline=False)
    embed.add_field(name=f"3️⃣ ---> Redes de Datos.", value='\u200b', inline=False)
    tmp = await channel.send(embed=embed)
    await tmp.add_reaction('1️⃣')
    await tmp.add_reaction('2️⃣')
    await tmp.add_reaction('3️⃣')
    def check1(reaction, user):
        return user == ctx.author  
    try:
        reaction, user = await client.wait_for('reaction_add', timeout=60.0,check=check1)
    except asyncio.TimeoutError:
            embed=discord.Embed(title=f"Se escogio de forma aleatoria.",color=discord.Color.red())
            await channel.send(embed=embed)
            name='Contabilidad y Costos'
            cursor.execute("SELECT pregunta,alternativa_a,alternativa_b,alternativa_c,alternativa_d,alternativa_correcta FROM PREGUNTAS_CONTABILIDAD")
            rows = cursor.fetchall()
    else:
        print(reaction)
        if '1️⃣'==reaction.emoji:
            name='Contabilidad y Costos'
            cursor.execute("SELECT pregunta,alternativa_a,alternativa_b,alternativa_c,alternativa_d,alternativa_correcta FROM PREGUNTAS_CONTABILIDAD")
            rows = cursor.fetchall()
        elif '2️⃣'==reaction.emoji:
            name='Programación'
            cursor.execute("SELECT pregunta,alternativa_a,alternativa_b,alternativa_c,alternativa_d,alternativa_correcta FROM PREGUNTAS_PROGRAMACION")        
            rows = cursor.fetchall()
        elif '3️⃣'==reaction.emoji:
            name='Redes de datos'
            cursor.execute("SELECT pregunta,alternativa_a,alternativa_b,alternativa_c,alternativa_d,alternativa_correcta FROM PREGUNTAS_REDESDEDATOS")
            rows = cursor.fetchall()
    await tmp.delete()

    emoji_a = '\N{Regional Indicator Symbol Letter A}'
    emoji_b = '\N{Regional Indicator Symbol Letter B}'
    emoji_c = '\N{Regional Indicator Symbol Letter C}'
    emoji_d = '\N{Regional Indicator Symbol Letter D}'
    cont=0
    i=0
    L = list(nivel)
    print(nivel[0])
    class Emojidis(Exception): pass
    for data in rows:
        i+=1
        embed=discord.Embed(title=f"Evaluación de {name}", description = f"Pregunta {i}",color=discord.Color.red())
        embed.add_field(name=f"{data[0]}", value ='\u200b', inline=False)
        embed.add_field(name=f"{emoji_a}   {data[1]}", value='\u200b', inline=False)
        embed.add_field(name=f"{emoji_b}   {data[2]}", value='\u200b', inline=False)
        embed.add_field(name=f"{emoji_c}   {data[3]}", value='\u200b', inline=False)
        embed.add_field(name=f"{emoji_d}   {data[4]}", value='\u200b', inline=False)
        embed.set_footer(icon_url= client.user.avatar_url , text="Para lista de comandos, ingresa !udp help")
        tmp = await channel.send(embed=embed)
        await tmp.add_reaction(emoji_a)
        await tmp.add_reaction(emoji_b)
        await tmp.add_reaction(emoji_c)
        await tmp.add_reaction(emoji_d)
        def check(reaction, user):
            aux=data[5]
            if user == ctx.author and str(reaction.emoji)==aux:
                raise Emojidis()
            else:
                return user == ctx.author and str(reaction.emoji)!=aux
        try:
            reaction, user = await client.wait_for('reaction_add', timeout=15.0, check=check)
            print(user)
            print(reaction)
            print(data[5])
        except Emojidis :
            cont+=1
            embed=discord.Embed(title=f"Alternativa correcta :partying_face:",color=discord.Color.red())
            message = await channel.send(embed=embed)
        else :
            embed=discord.Embed(title=f"Alternativa incorrecta.",color=discord.Color.red())
            message = await channel.send(embed=embed)
       

        await tmp.delete()
        time.sleep(5) 
        await message.delete()
    if cont>=7:
        L[0]+=1
        cursor.execute(f"UPDATE USUARIOS SET nivel={L[0]} WHERE id_discord ={member.id}")
        embed=discord.Embed(title=f"Felcidades subiste de nivel. :muscle: ", description = f"Tu nivel actual es: {L[0]}",color=0xe65ba5)
        embed.set_footer(icon_url = client.user.avatar_url, text = f"{ctx.author.name}")
        await channel.send(embed=embed)
    else:
        embed=discord.Embed(title=f"Lo siento pero no alcansaste el minimo de preguntas correctas. :sob: ", description = f"Te recomiendo que estudies de las catedras !udp Catedra",color=0xe65ba5)
        embed.set_footer(icon_url = client.user.avatar_url, text = f"{ctx.author.name} ")
        await channel.send(embed=embed)
    udp_worlddb.commit()        
    cursor.close()        
    udp_worlddb.close()

#comando de admin;no usar pls
@client.command()
async def dilit(ctx,*,member: discord.Member=None):
    if not member:
        member = ctx.author
    udp_wolrddb = sqlite3.connect('udp_world.sqlite')
    cursor = udp_wolrddb.cursor()
    cursor.execute(f"SELECT id_discord FROM INVENTARIO WHERE id_discord = {member.id}")
    result = cursor.fetchone()
    print(result)
    if result is None:
        await ctx.send("No tiene ningun personaje creado :c")
        return
    else :
        cursor.execute(f"DELETE FROM USUARIOS WHERE id_discord = {member.id} ")
        cursor.execute(f"DELETE FROM INVENTARIO WHERE id_discord = {member.id} ")
        cursor.execute(f"DELETE FROM JUGADOR WHERE id_discord = {member.id} ")
        await ctx.send("Usuario Eliminado :D")               
    #Commit
    udp_wolrddb.commit()
    #Cursor.close
    cursor.close()
    #DB close
    udp_wolrddb.close()

#confirma que inicia el bot
@client.event
async def on_ready():
    db = sqlite3.connect('udp_world.sqlite')
    cursor = db.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS JUGADOR(
        id_discord integer PRIMARY KEY
        )
        ''')
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS INVENTARIO(
        id_discord integer NOT NULL,
        id_invetario TEXT PRIMARY KEY,
        medicamentos TEXT NOT NULL,
        computadora TEXT NOT NULL,
        apuntes TEXT NOT NULL,
        comida TEXT NOT NULL,
        FOREIGN KEY (id_discord) REFERENCES JUGADOR (id_discord)
        )
        ''')
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS USUARIOS(
        ID_Perfil TEXT PRIMARY KEY,
        nombre TEXT NOT NULL,
        nivel INTEGER NOT NULL,
        carrera TEXT NOT NULL,
        id_discord integer NOT NULL,
        FOREIGN KEY (id_discord) REFERENCES JUGADOR (id_discord)
        )
        ''')
    
    
    print(f'We have logged in as {client.user}')
    return await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name = '!udp ayuda para comandos'))

#inicia el bot
client.run('Nzc1ODY1OTQ4MzIyMTM2MDY2.X6sjeA.lZCmNAN6Luwayddwhkao_GIQ_GU')